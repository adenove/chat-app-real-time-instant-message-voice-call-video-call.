import socket,{message} from '../socket';

const saveMessage = (msg) => {
    const id = msg.sender !== socket.id ? msg.sender : msg.to;
    const chekmessage = message.find(ms => ms.from === id )
    if(chekmessage === undefined){
        const tosave = {from:id,chat:[msg]};
        message.push(tosave);
        return message;
    }else{
        chekmessage.chat.push(msg);
        return message;
    }
}

const getMessage = (id) => {
    if(id !== ""){
        try {
            const getmsgId = message.find(msg => msg.from === id );
            return getmsgId.chat;
        } catch (error) {
            return [];
        }
    }else{
        return [];
    }
}

export {saveMessage,getMessage};