import io from 'socket.io-client';
import { Notify } from '../../container/Main';
import { newMessage,selectUser } from '../../container/home/eventEmmiter';
import { saveMessage } from '../socket/socketmanager';

const Nama = {nama:''};

const socket = io('https://chatserverku.herokuapp.com/');

const message = [];

socket.on('message',(data)=>{
    switch (data.type) {
        case 'message':
            saveMessage(data)
            Notify(data);
            newMessage();
            break;
        default:
            break;
    }
});

socket.on('userConnect',data =>{
    switch (data.type) {
        case 'offer':
            socket.emit('connectID',{type:'answer',sender:socket.id,to:data.sender,name:Nama.nama});
            break;
        case 'answer':
            selectUser(data.sender,data.name);
            break;
        default:
            break;
    }
})

export { Nama,message };

export default socket;