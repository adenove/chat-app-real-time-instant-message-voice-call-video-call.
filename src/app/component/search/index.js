import React,{useState} from 'react';
import { Paper, InputBase, Button } from '@material-ui/core';
import socket from '../../config/socket';
import Grid from '@material-ui/core/Grid';

import './style.css';

const Search = () => {

    const [idUser,setIdUser] = useState('');

    const handleClick = () => {
        if(socket.id !== idUser) socket.emit('connectID',{type:'offer',sender:socket.id,to:idUser})
        else alert('jangan ngechat diri sendiri ,dasar jomblo');
    }

    const handleInput = (e) => {
        setIdUser(e.target.value);
    }

    return(
        <div id="search" style={{display:'flex',marginBottom:50}} >
            <Grid container spacing={3} style={{display:'flex',alignItems:'center',justifyContent:'center'}} >
                <Grid item xs={12} md={10} >
                    <Paper style={{flex:2}} >
                        <InputBase
                            onChange={handleInput}
                            style={{padding:'10px 10px 5px 10px',width:'100%'}}
                            placeholder="Input ID Friend"
                        />
                    </Paper>
                </Grid>
                <Grid item xs={12} style={{display:'flex',justifyContent:'center'}} md={2} >
                    <Button
                        onClick={handleClick}
                        variant="contained"
                        color="primary">
                        <b>connect</b>
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}

export default Search;