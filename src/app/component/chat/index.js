import React from 'react';
import Paper from '@material-ui/core/Paper';
import './style.css';
import socket from '../../config/socket';

const Chat = (props) => {
    return (
        <div className={`chat-box ${ props.id === socket.id ? `right` : `left` }`} >
            <Paper style={{padding:10,maxWidth:'90%'}} >
                <p className="name" ><b>{props.name}</b></p>
                <p className="message">{props.message}</p>
            </Paper>
        </div>
    )
}

export default Chat;