import { EventEmitter } from 'events';

const ee = new EventEmitter();

export const openContact = () => {
  ee.emit('open');
}

export const closeContact = () => {
	ee.emit('close',);
}

export default ee;