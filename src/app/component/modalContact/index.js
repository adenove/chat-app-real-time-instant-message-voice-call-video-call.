import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ee from './EventEmitter';

const ModalContact = () => {
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  ee.on('open',()=>{
      setOpen(true);
  })

  ee.on('close',()=>{
      setOpen(false);
  })

  return (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title" style={{background:'#3f51b5',color:'white',textAlign:'center'}} >{"Connect Us"}</DialogTitle>
        <DialogContent>
          <DialogContentText style={{padding:'10px 30px',display:'flex',alignItems:'center',justifyContent:'center'}} id="alert-dialog-description">
            <b>adenovendra19@gmail.com</b>
          </DialogContentText>
        </DialogContent>
      </Dialog>
  );
}

export default ModalContact;