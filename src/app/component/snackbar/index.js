import React from 'react';
import { Paper,Button,IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import './snackbar.css';


const excerpt = (string) => {
    return string.substring(0,10) + (string.length > 10 ? '...' : '' );
}

const Snackbar = (props) => {
    return(
        <Paper
            style={{background:'#434343',justifyContent:'space-between'}}
            className="notify"
            href="#"
            variant="extended"
            aria-label="add" >
            <h4>{props.name}</h4>
            <p>{excerpt(props.message)}</p>
            <Button
                onClick={()=>props.open(props.from,props.name)}
                style={{color:'#68ff7cde'}} >
                <b>Open</b>
            </Button>
            <IconButton
                style={{color:'#ff6d6d'}}
                onClick={()=>props.close(props.index)}
                size="small" >
                <CloseIcon fontSize="inherit" />
            </IconButton>
        </Paper>
    )
}

export default Snackbar;