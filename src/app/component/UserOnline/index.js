import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ee from './Eventemmiter';
import socket from '../../config/socket';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import { IconButton } from '@material-ui/core';

const UserOnline = () => {
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  ee.on('open',()=>{
      setOpen(true);
  })

  ee.on('close',()=>{
      setOpen(false);
  })


  const copyClipboard = () => {
    const textFiled = document.createElement('textarea');
    textFiled.innerText = socket.id;
    const parrenElm = document.getElementById('alert-dialog-description');
    parrenElm.appendChild(textFiled);
    textFiled.select();
    document.execCommand('copy');
    alert('copied')
    parrenElm.removeChild(textFiled);
  }

  return (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Your id"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          <IconButton onClick={copyClipboard} >
            <FileCopyIcon/>
          </IconButton>
            <b>{socket.id}</b>
            <br/>
            <br/>
            <b>Share Your id to Connect Friend </b>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            close
          </Button>
        </DialogActions>
      </Dialog>
  );
}

export default UserOnline;