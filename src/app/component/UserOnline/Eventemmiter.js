import { EventEmitter } from 'events';

const ee = new EventEmitter();

export const openUser = () => {
  ee.emit('open');
}

export const closeUser = () => {
	ee.emit('close',);
}

export default ee;