import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import CallIcon from '@material-ui/icons/Call';
import { Dialing } from '../../container/calling-dialog/eventEmmiter';
import {openUser} from '../UserOnline/Eventemmiter';
import { selectUser } from '../../container/home/eventEmmiter';
import HomeIcon from '@material-ui/icons/Home';
import './style.css';

const Header = (props) => {

    return (
      <div style={{height:'auto'}} >
        <AppBar position="static" style={{padding:5}} >
            <Toolbar>
            <div className="iconUser" >
              <IconButton onClick={openUser} edge="start" color="inherit" aria-label="menu">
                  <SupervisedUserCircleIcon />
              </IconButton>
            </div>
            <Typography variant="h6" color="inherit" style={{width:'100%'}} >
                { props.name === "" ? 'Chat App' : props.name }
            </Typography>
              {
                props.id !== "" ?
              <div className="ui" style={{width:'100%',display:'flex',justifyContent:'flex-end'}} >
              <IconButton onClick={()=>selectUser('','')} edge="start" color="inherit" aria-label="menu">
                  <HomeIcon />
              </IconButton>
                <IconButton
                  onClick={()=> Dialing(props.id,props.name,'voice') }
                  color="inherit">
                  <CallIcon />
                </IconButton>
                <IconButton
                  onClick={()=> Dialing(props.id,props.name,'video') }
                  color="inherit">
                  <VideoCallIcon />
                </IconButton>
              </div> :
                ''
              }
            </Toolbar>
        </AppBar>
      </div>
    )
  }

export default Header;