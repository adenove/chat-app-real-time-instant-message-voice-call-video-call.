import React,{useState} from 'react';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import {saveMessage} from '../../config/socket/socketmanager';
import Button from '@material-ui/core/Button';
import socket,{Nama} from '../../config/socket';
import { newMessage } from '../../container/home/eventEmmiter';
import './style.css';

const InputMessage = (props) => {

    const [message,setMessage] = useState('');

    const inputChange = (e) => {
        setMessage(e.target.value);
    }

    const handleSend =() => {
		const msg = {type:'message',sender:socket.id,to:props.to,nama:Nama.nama,message:message};
		socket.emit('chatMessage',JSON.stringify(msg));
        saveMessage(msg);
        setMessage('');
        newMessage();
	}

    return (
        <div className="bottom-panel" >
            <TextareaAutosize
                onChange={inputChange}
                value={message}
                name="message"
                className="input-message"
                rowsMax={4}
                placeholder="Tulis Pesan"
                />
            <Button
                onClick={handleSend}
                style={{marginRight:50}}
                variant="contained"
                size="medium"
                color="primary">
                Send
            </Button>
        </div>
    )
}

export default InputMessage;