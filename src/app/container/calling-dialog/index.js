import React,{Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CallIcon from '@material-ui/icons/Call';
import ee from './eventEmmiter';
import socket,{Nama} from '../../config/socket';
import './style.css';


class CallingDialog extends Component {

  constructor(props){
    super(props);
    this.state = {
      id:'',
      name:'',
      isme:false,
      open:false,
      quote:'',
      connect:false,
      typeCall:'',
      reject:false
    }

    this.server = {iceServers: [
      {'urls': 'stun:stun.services.mozilla.com'},
      {'urls': 'stun:stun.l.google.com:19302'},{
      "urls": [
      "turn:13.250.13.83:3478?transport=udp"
      ],
      "username": "YzYNCouZM1mhqhmseWk6",
      "credential": "YzYNCouZM1mhqhmseWk6"
      },
      {
        url: 'turn:numb.viagenie.ca',
        credential: 'muazkh',
        username: 'webrtc@live.com'
      }
    ]
  };
    this.youVid = React.createRef();
    this.friendVid = React.createRef();
    this.stream = "";
    this.clienStream = "";
    this.test = "";
  }

openVideocall = (credential = "") => new Promise((resolve,reject)=>{
  var setting;
  if(credential === 'voice'){
    setting = {audio:true,video:false}
  }else{
    setting = {audio:true,video:true}
  }

  try {
    this.pc = new RTCPeerConnection(this.server);
    navigator.mediaDevices.getUserMedia(setting).then(stream => {
      this.pc.addStream(stream);
      resolve(stream);
    }).catch(e => reject(e))
    this.initStream();
  } catch (error) {
    alert('webcam  or microphone not allowed');
  }
})

componentDidMount(){
  this.initEmmit();
  this.initSocket();
}

initEmmit = () => {
  ee.on('dialing',(id,name,credential)=>{
    if(!this.state.connect){
      this.setState({typeCall:credential},()=>{
        this.openVideocall(credential).then(stream => {
          this.stream = stream;
          this.setState({id:id,name:name,isme:true,open:true,quote:'menunggu Jawaban',reject:false},()=>{
            this.pc.createOffer().then(offer => {
              this.youVid.current.srcObject = stream;
              this.pc.setLocalDescription(new RTCSessionDescription(offer));
              return offer;
            }).then((offer)=>{
              socket.emit('calling',JSON.stringify({sender:socket.id,to:id,name:Nama.nama,sdp:offer,setting:credential}));
              this.test = setTimeout(() => {
                  clearTimeout(this.test);
                  if(!this.state.connect && !this.state.reject){
                    this.rejectCall();
                    alert('tidak ada jawaban');
                  }
              }, 20000);
            })
          });
        });
      });
    }else{
      console.log('sedang berada di panggilan');
    }
  });
}

initStream = () => {
  this.pc.onicecandidate = e => {
    if(e.candidate){
      socket.emit('calling',JSON.stringify({sender:socket.id,to:this.state.id,sdp:{type:'candidate',candidate:e.candidate}}));
    }
  }
  this.pc.onaddstream = e => {
    this.clienStream = e.stream;
    this.friendVid.current.srcObject = e.stream;
  }
}

initSocket = () => {
  socket.on('calling',async(data) => {
    const parse = JSON.parse(data);
    switch (parse.sdp.type) {
      case 'candidate':
          try {
           await this.pc.addIceCandidate(new RTCIceCandidate(parse.sdp.candidate));
          } catch (error) {
            // console.log('have some litle bug :)');
          }
        break;
      case 'offer':
          if(!this.state.connect){
            this.setState({typeCall:parse.setting},()=>{
              this.openVideocall(parse.setting).then(stream => {
                this.stream = stream;
                this.setState({id:parse.sender,name:parse.name,isme:false,open:true,quote:parse.name + ' sedang menunggu Jawaban anda' },()=>{
                  this.pc.setRemoteDescription(parse.sdp).then(()=>{
                    this.youVid.current.srcObject = stream;
                  })
                });
              })
            })
          }else{
            socket.emit('calling',JSON.stringify({sender:socket.id,to:parse.sender,sdp:{type:'close'}}));
          }
        break;
      case 'answer':
        this.pc.setRemoteDescription(parse.sdp);
        this.setState({connect:true,quote:'anda sudah terhubung dengan '+ this.state.name});
        break;
      case 'close':
        this.closeConnection();
        alert('panggilan telah diakhiri');
        break;
      default:
        console.log('default')
        break;
    }
  });
}

closeConnection = () => {
  this.setState({open:false,connect:false,reject:true})
  try {
    const video = this.stream.getTracks();
    video.forEach(v => {
      v.stop();
    });

    if(this.clienStream !== ""){
      try {
        this.pc.removeStream(this.clienStream);
        this.clienStream = "";
      } catch (error) {
        console.log('eror');
      }
    }
    this.pc.close();
  } catch (error) {
    alert('Camera and microphone access may not be permitted')
  }
}

rejectCall = () => {
  this.closeConnection();
  socket.emit('calling',JSON.stringify({sender:socket.id,to:this.state.id,sdp:{type:'close'}}));
}

sendAnswer = () => {
  this.pc.createAnswer().then(answer => {
    this.pc.setLocalDescription(new RTCSessionDescription(answer))
    .then(()=>{
      socket.emit('calling',JSON.stringify({sender:socket.id,to:this.state.id,sdp:answer}));
      this.setState({isme:true,connect:true,quote:'Terhubung dengan '+this.state.name})
    });
  });
}

ButtonAnswer = () => {
  return <Button style={{width:'100%',color:'#4ebd52'}} onClick={this.sendAnswer} >
          <b>Answer</b>
        </Button>
}

  render(){
    return(
      <div>
        <Dialog
          fullScreen={ this.state.typeCall === 'voice' ? false : true }
          open={this.state.open}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          { this.state.typeCall === 'voice' ?
             <DialogTitle
              style={{background:'#3f51b5',color:'white',textAlign:'center'}}
              id="alert-dialog-title">
              <CallIcon fontSize="inherit" style={{marginRight:20,position:'relative',top:3}} />
              {this.state.name}
          </DialogTitle> : ''}
          <DialogContent>
            <DialogContentText
              style={{display:'flex',justifyContent:'center',alignItems:'center',padding:'10px',position:'relative',flexDirection:'column',textAlign:'center',fontWeight:600}}
                id="alert-dialog-description">
              {
                this.state.typeCall === 'voice' ?
                <React.Fragment>
                  <video id="music" ref={this.youVid} autoPlay muted></video>
                  <video id="music" ref={this.friendVid} autoPlay ></video>
                </React.Fragment>:
                <React.Fragment >
                  <video style={{width:'100%',height:'80vh',background:'black'}} ref={this.friendVid} autoPlay></video>
                  <video style={{width:'10vh',height:'10vh',background:'black',position:'absolute',top:30,left:30}} ref={this.youVid} autoPlay muted></video>
                </React.Fragment>
              }
              <br/>
              {this.state.quote}
            </DialogContentText>
          </DialogContent>
          <DialogActions style={{display:'flex',justifyContent:'center'}} >
            { this.state.isme !== true ? this.ButtonAnswer() : '' }
            <Button onClick={this.rejectCall} color="secondary" style={{width:'100%'}} >
              <b>Reject</b>
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default CallingDialog;