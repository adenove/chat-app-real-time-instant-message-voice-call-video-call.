import { EventEmitter } from 'events';

const ee = new EventEmitter();

export const Dialing = (id,name,credential) => {
    ee.emit('dialing',id,name,credential);
}

export const Reject = () => {
    ee.emit('reject');
}

export const Respond = (data) => {
    ee.emit('respond',data);
}

export default ee;