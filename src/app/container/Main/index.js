import React from 'react';
import { EventEmitter } from 'events';
import Snackbar from '../../component/snackbar';
import {openMessage} from '../home/eventEmmiter';
import './main.css';
import CallingDialog from  '../calling-dialog';
import UserOnline from '../../component/UserOnline';

const ee = new EventEmitter();
const isHttp = 'https:' + window.location.href.substring(window.location.protocol.length);

export const Notify = (data) => {
    ee.emit('Notify',data);
}

class Main extends React.Component {

    constructor(props){
        super(props);
        this.initEmmit();
        this.notification = [];
        this.video = '';
        this.videoTag = React.createRef();
    }

    initEmmit = () => {
        ee.on('Notify',(data)=>{
            this.notification.push({name:data.nama,message:data.message,from:data.sender})
            this.setState({new:'new'});
        })
    }

    handleOpen = (id,name) => {
        openMessage(id,name);
        let length = this.notification.length;
        const getnotif = []
        for (let index = 0; index < length; index++) {
            if(this.notification[index].from !== id )
            {
                getnotif.push(this.notification[index]);
            }
        }
        this.notification = getnotif;
        this.setState({new:'open'});
    }

    handleClose = (index) => {
        this.notification.splice(index,1);
        this.setState({new:'delete'})
    }

    UNSAFE_componentWillMount(){
        if(window.location.protocol !== "https:") window.location.href = isHttp;
    }

    render(){
        return(
            <React.Fragment>
                <CallingDialog/>
                <UserOnline />
                <div className="box" >
                    {
                        this.notification.map((value,index)=>{
                            return <Snackbar
                                        key={index}
                                        index={index}
                                        name={value.name}
                                        message={value.message}
                                        open={this.handleOpen}
                                        close={this.handleClose}
                                        from={value.from} />
                        })
                    }
                </div>
                {this.props.children}
            </React.Fragment>
        )
    }
}

export default Main;