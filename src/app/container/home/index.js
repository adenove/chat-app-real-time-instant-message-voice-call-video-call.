import React,{Component} from 'react';
import socket,{Nama} from '../../config/socket';
import {getMessage} from '../../config/socket/socketmanager';
import Header from '../../component/header';
import InputMessage from '../../component/inputMessage';
import Chat from '../../component/chat';
import Search from '../../component/search';
import ee from './eventEmmiter';
import './style.css';

class Home extends Component{

  constructor(props){
	  super(props);

		//chek login
		if(Nama.nama === "") {
			this.props.history.push('/login');
		}

		this.state = {
			nama:Nama.nama,
			message:'',
			chat:[],
			friendName:'',
			idFriend:'',
			scroll:false
  	}

    this.chat = [];
	  this.main = React.createRef();
	  this.scrlTop = "";
  }

  	clickOnlinePeople = (id,name) => {
	  this.setState({idFriend:id,friendName:name,scroll:true},()=>{
		  this.scrollToBottom();
	  });
  	}

  	scrollToBottom = () => {
			if(this.state.scroll === true){
				this.main.current.scrollTop = this.main.current.scrollHeight;
				this.scrollTop = this.main.current.scrollTop;
			}
	  };

	scrollEvent = () => {
		const scrl = document.querySelector('.content-chat');
		scrl.addEventListener('scroll',()=>{
			if(this.scrollTop !== scrl.scrollTop){
				this.setState({scroll:false});
			}
			if(this.scrollTop === scrl.scrollTop){
				this.setState({scroll:true});
			}
		});
	}

	initialEmmitEvent = () => {
		ee.on('newMessage',()=>{
        this.setState({refresh:'refresh'},()=>{
        	this.scrollToBottom();
			});
		});

		ee.on('openMessage',(id,name) => {
			this.clickOnlinePeople(id,name);
		})

		ee.on('selectUser',(id,name)=>{
			this.clickOnlinePeople(id,name);
		})
	}

	UNSAFE_componentWillMount(){
		socket.emit('getclient');
	}

    componentDidMount(){
			this.scrollEvent();
			this.initialEmmitEvent();
		}

    render() {
        return (
          <div className="container" >
						{/* <Sidebar	handle={this.clickOnlinePeople} /> */}
              <div className="main">
								<Header id={this.state.idFriend} name={this.state.friendName} />
            			<div ref={this.main} className={`content-chat ${this.state.idFriend === "" ? 'centercontent' : '' }`} >
									{
										getMessage(this.state.idFriend).map((value,key)=>{
											return (
												<Chat
													key={key}
													id={value.sender}
													name={value.nama}
													message={value.message} />
											)
										})
									}
									{this.state.idFriend === "" ? <React.Fragment><Search /> <img style={{width:'50%'}} src={require('../../../assets/call.svg')} alt="icon" /></React.Fragment> : '' }
            		</div>
								{this.state.idFriend !== "" ? <InputMessage to={this.state.idFriend} /> : ""}
              </div>
          </div>
        )
    }
}

export default Home;