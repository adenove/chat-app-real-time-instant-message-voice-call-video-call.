import { EventEmitter } from 'events';

const ee = new EventEmitter();

export const newMessage = () => {
  ee.emit('newMessage');
}

export const openMessage = (id,name) => {
	ee.emit('openMessage',id,name);
}

export const selectUser = (id,name) => {
  ee.emit('selectUser',id,name);
}

export default ee;