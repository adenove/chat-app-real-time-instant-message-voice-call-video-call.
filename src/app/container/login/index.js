import React,{Component} from 'react';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Box, TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import socket, { Nama } from '../../config/socket';
import {openContact} from '../../component/modalContact/EventEmitter';
import ModalContact from '../../component/modalContact';

const styles = theme => ({
    '@global': {
		body: {
			backgroundColor: '#3f51b5',
		},
    },
    root: {
      height:'100vh',
      display:'flex',
      justifyContent:'center',
      alignItems:'center',
      flexDirection:'column'
    },
    paper: {
      padding: theme.spacing(5),
      textAlign: 'center',
      backgroundColor:'#f3f4f8'
    },
    button:{
        marginTop:20
    },
    form:{
		display:'flex',
		flexDirection:'column',
		paddingBottom:20,
		minWidth:250
    },
    inputName:{
        padding:0
    }
  });

class Login extends Component {

	constructor(props){
		super(props);
		this.state = {
			nama:''
        }
	}

	handleInput = (e) =>{
		this.setState({[e.target.name]:e.target.value});
	}

	handleSubmit = (e) => {
        e.preventDefault();
        socket.emit('createUser',this.state.nama);
		Nama.nama = this.state.nama;
		this.props.history.push('/')
    }

    UNSAFE_componentWillMount(){
		if(Nama.nama !== ""){
            this.props.history.push('/');
        }
    }

    render(){
        const { classes } = this.props;
        return (
            <Container className={classes.root} maxWidth="md" >
                <ModalContact />
                <Box>
                    <Paper className={classes.paper}>
                        <Box className={classes.title} >
                            <p><b>Masukan Nama mu</b></p>
                        </Box>
                        <Box>
                            <form onSubmit={this.handleSubmit} className={classes.form} >
                                <TextField
                                    id="outlined-email-input"
                                    label="Name"
                                    className={classes.inputName}
                                    type="text"
                                    name="nama"
                                    autoComplete="off"
                                    margin="normal"
									onChange={this.handleInput}
                                    variant="outlined" />
                                <Button
									type="submit"
                                    variant="contained"
                                    color="secondary"
                                    className={classes.button}
									size="large"
									onClick={this.handle}
                                    >
                                    Submit
                                </Button>
                            </form>
                        </Box>
                    </Paper>
                </Box>
                <Button onClick={openContact} style={{marginTop:50,color:'white',fontWeight:600,textTransform:'capitalize'}} >
                    Contact Us
                </Button>
            </Container>
        )
    }
}

export default withStyles(styles)(Login);