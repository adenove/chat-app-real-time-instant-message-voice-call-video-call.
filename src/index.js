import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Main from './app/container/Main';
// import Home from './app/container/home';
// import Login from './app/container/login';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Main><App /></Main>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
