import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Login from './app/container/login';
import Home from './app/container/home';

function App() {
  return (
    <Router>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
    </Router>
  );
}

export default App;
